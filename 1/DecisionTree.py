# -*- coding: utf-8 -*-
#
# Created by Xinyu on 2019/9/25 20:19
#

import pandas as pd
import numpy as np
from collections import defaultdict
from Plot import Plot


def I(data: pd.DataFrame, result: str) -> float:
    '''
    计算信息值
    :param data: 数据集
    :param result: 要预测的类
    :return: 信息值
    '''
    cnt: float = 0
    data_cnt: int = data[result].count()
    all_result_cnt: list = data[result].value_counts()
    for i in all_result_cnt:
        p: float = i / data_cnt
        cnt += (- p * np.log2(p))
    return cnt


def E(data: pd.DataFrame, col: str, result: str) -> float:
    '''
    计算子集熵
    :param data: 数据集
    :param col: 某个类
    :param result: 要预测的类
    :return: 熵
    '''
    cnt: float = 0
    data_cnt: int = data[col].count()
    all_col_cnt: list = data[col].value_counts()
    all_col_val: list = all_col_cnt.index
    for i, j in zip(all_col_val, all_col_cnt):
        p: float = j / data_cnt
        cnt += p * I(data[data[col] == i], result)
    return cnt


def Gain(data: pd.DataFrame, col: str, result: str) -> float:
    '''
    计算信息增量
    :param data: 数据集
    :param col: 某个类
    :param result: 要预测的类
    :return: 信息增量
    '''
    return I(data, result) - E(data, col, result)


def SplitInfo(data: pd.DataFrame, col: str) -> float:
    '''
    计算信息增益比例需要的熵
    :param data: 数据集
    :param col: 某个类
    :return: 熵
    '''
    cnt: float = 0
    data_cnt: int = data[col].count()
    all_col_cnt: list = data[col].value_counts()
    for i in all_col_cnt:
        p: float = i / data_cnt
        cnt += (- p * np.log2(p))
    return cnt


def GainRadio(data: pd.DataFrame, col: str, result: str) -> float:
    '''
    计算信息增益比例
    :param data: 数据集
    :param col: 某个类
    :param result: 要预测的类
    :return: 信息增益比例
    '''
    return Gain(data, col, result) / SplitInfo(data, col)


def processNumber(data: pd.DataFrame, col: str, result: str) -> pd.DataFrame:
    '''
    处理连续值
    :param data: 数据集
    :param col: 某个类
    :param result: 要预测的类
    :return: 处理连续集后的数据集
    '''
    data.sort_values(col, inplace=True, ascending=True)
    all_val = np.unique(data[col])
    all_val_mean = []
    for i in range(len(all_val) - 1):
        all_val_mean.append(np.mean([all_val[i], all_val[i + 1]]))
    all_val_mean = np.array(all_val_mean)
    max_gr = 0; max_i = 0
    for i in all_val_mean:
        select = pd.cut(data[col], bins=[0, i, np.inf])
        new_data = pd.DataFrame({col:select, result: data[result]})
        if GainRadio(new_data, col, result) > max_gr:
            max_gr = GainRadio(new_data, col, result); max_i = i
    del new_data, all_val_mean
    data[col] = pd.cut(data[col], bins=[0, max_i, np.inf])
    data[col] = data[col].astype(str)
    return data


def decideNode(data: pd.DataFrame, result: str):
    '''
    用信息增益比例决定结点
    :param data: 数据集
    :param result: 要预测的类
    :return: 结点名字，结点信息增益比例
    '''
    max_gr = 0; max_i = 0
    for i in data.columns[0: -1]:
        if GainRadio(data, i, result) > max_gr:
            max_gr = GainRadio(data, i, result);  max_i = i
    return max_i, max_gr


def tree():
    '''
    构造树结构
    :return: 树
    '''
    return defaultdict(tree)


def DecisionTree(data: pd.DataFrame, result: str, cols: list) -> defaultdict:
    '''
    建立决策树
    :param data: 数据集
    :param result: 要预测的类
    :param cols: 选择的列名列表
    :return: 决策树
    '''
    # 只有一种结果
    if data[result].value_counts().count() == 1:
        return data[result].value_counts().index[0]
    # 只剩标签
    if len(cols) == 1:
        return data["去玩？"].value_counts().index[0]
    # 选择特征
    node_i, node_gr = decideNode(data, "去玩？")
    node_val = np.unique(data[node_i])
    # 回溯法建树
    cTree = tree()
    for i in node_val:
        cols.remove(node_i)
        cTree[node_i][i] = DecisionTree(data[data[node_i] == i][cols], result, cols)
        cols.insert(0, node_i)
    return cTree


if __name__ == "__main__":
    # 读取数据
    dataset = pd.read_csv("dataset.csv")
    # 预处理
    dataset.dropna(inplace=True) # 丢弃空值
    dataset = processNumber(dataset, "湿度", "去玩？") # 处理连续值
    # 建立决策树
    dt = DecisionTree(dataset, "去玩？", dataset.columns.to_list())
    # 打印决策树
    Plot(dt, False)
