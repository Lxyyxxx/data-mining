# -*- coding: utf-8 -*-
#
# Created by Xinyu on 2019/9/26 12:03
#

import matplotlib.pyplot as plt
import json


# 全局样式
decision_node = dict(boxstyle="sawtooth", fc="0.8")
leaf_node = dict(boxstyle="round4", fc="0.8")
arrow_args = dict(arrowstyle="<-")


def defaultdict2dict(tree) -> dict:
    '''
    defaultdict转字典
    :param tree: defaultdict形式的树
    :return: 字典形式的树
    '''
    return json.loads(json.dumps(tree))


def getLeafsNumber(tree: dict) -> int:
    '''
    计算叶子结点个数
    :param tree: 字典形式的树
    :return: 叶子结点个数
    '''
    cnt = 0
    first_key = list(tree.keys())[0]
    sub_tree = tree[first_key]
    for key in sub_tree.keys():
        cnt += getLeafsNumber(sub_tree[key]) if type(sub_tree[key]).__name__ == "dict" else 1
    return cnt


def getTreeDepth(tree: dict) -> int:
    '''
    计算树的深度
    :param tree: 字典形式的树
    :return: 树的深度
    '''
    max_depth = 0
    first_key = list(tree.keys())[0]
    sub_tree = tree[first_key]
    for key in sub_tree.keys():
        depth = 1 + getTreeDepth(sub_tree[key]) if type(sub_tree[key]).__name__ == "dict" else 1
        max_depth = depth if depth > max_depth else max_depth
    return max_depth


def createPlot(tree: dict, save: bool = True, filepath: str = "tree.png") -> None:
    '''
    用matplotlib打印字典形式的树
    :param tree: 字典形式的树
    :param save: 是否需要保存图片，默认为是
    :param filepath: 保存图片的地址，默认为tree.png
    :return: 无
    '''
    fig = plt.figure(1, facecolor='white')
    fig.clf()
    ax_props = dict(xticks=[], yticks=[])
    createPlot.ax = plt.subplot(111, frameon=False, **ax_props)
    plotTree.total_width = float(getLeafsNumber(tree))
    plotTree.total_depth = float(getTreeDepth(tree))
    plotTree.x_off = - 0.5 / plotTree.total_width;
    plotTree.y_off = 1.0
    plotTree(tree, (0.5, 1.0), "")
    if save: plt.savefig(filepath)
    plt.show()


def plotTree(tree: dict, parent_point: tuple, node_info: str) -> None:
    '''
    打印树
    :param tree: 字典形式的树
    :param parent_point: 父结点的坐标
    :param node_info: 结点信息
    :return: 无
    '''
    leafs_num = getLeafsNumber(tree)
    first_key = list(tree.keys())[0]
    center_point = (plotTree.x_off + (1.0 + float(leafs_num)) / 2.0 / plotTree.total_width, plotTree.y_off)
    plotMidText(center_point, parent_point, node_info)
    plotNode(first_key, center_point, parent_point, decision_node)
    sub_tree = tree[first_key]
    plotTree.y_off -= 1.0 / plotTree.total_depth
    for key in sub_tree.keys():
        if type(sub_tree[key]).__name__ == "dict":
            plotTree(sub_tree[key], center_point, str(key))
        else:
            plotTree.x_off += 1.0 / plotTree.total_width
            plotNode(sub_tree[key], (plotTree.x_off, plotTree.y_off), center_point, leaf_node)
            plotMidText((plotTree.x_off, plotTree.y_off), center_point, str(key))
    plotTree.y_off += 1.0 / plotTree.total_depth


def plotMidText(center_point: tuple, parent_point: tuple, node_info: str) -> None:
    '''
    打印箭头中间的字，即特征取值
    :param center_point: 当前结点坐标
    :param parent_point: 父结点的坐标
    :param node_info: 结点信息
    :return: 无
    '''
    mid_x = (parent_point[0] + center_point[0]) / 2.0
    mid_y = (parent_point[1] + center_point[1]) / 2.0
    createPlot.ax.text(mid_x, mid_y, node_info)


def plotNode(node_info: str, center_point: tuple, parent_point: tuple, node_type: str) -> None:
    '''
    打印结点，即特征名
    :param node_info: 结点信息
    :param center_point: 当前结点坐标
    :param parent_point: 父结点的坐标
    :param node_type: 结点类型，根结点/中间结点还是叶子
    :return: 无
    '''
    createPlot.ax.annotate(node_info,
                     xy=parent_point, xycoords="axes fraction", xytext=center_point,
                     textcoords='axes fraction', va="center", ha="center",
                     bbox=node_type, arrowprops=arrow_args)


def Plot(tree, save: bool = True, filepath: str = "tree.png") -> None:
    '''
    打印defaultdict形式的树
    :param tree: defaultdict形式的树
    :param save: 是否需要保存图片，默认为是
    :param filepath: 保存图片的地址，默认为tree.png
    :return: 无
    '''
    tree = defaultdict2dict(tree)
    createPlot(tree, save, filepath)
