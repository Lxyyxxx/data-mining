from pyecharts.charts import Bar,Line
from pyecharts import options as opts
from pyecharts.globals import ThemeType
import pandas as pd

# 柱状图
def bar(data, title=None, subtitle=None, useCols=None,yName=None,xName=None,pos_left=None):
    barFig = Bar(init_opts=opts.InitOpts(theme=ThemeType.WESTEROS))
    barFig.set_global_opts(title_opts=opts.TitleOpts(title=title,subtitle=subtitle),
                           yaxis_opts=opts.AxisOpts(name=yName),
                           xaxis_opts=opts.AxisOpts(name=xName),
                           legend_opts=opts.LegendOpts(pos_left=pos_left))
    if isinstance(data, pd.Series):
        barFig.add_xaxis(data.index.tolist())
        barFig.add_yaxis(data.name, data.fillna(0).values.tolist())
    elif isinstance(data, pd.DataFrame):
        useCols = useCols if useCols else data.columns
        for i in range(len(data.columns)):
            barFig.add_xaxis(data[data.columns[i]].index.tolist())
            barFig.add_yaxis(str(useCols[i]), data[data.columns[i]].fillna(0).values.tolist())
    return barFig


# 折线图
def line(data,title=None, subtitle=None, useCols=None,yName=None,xName=None,pos_left=None):
    lineFig = Line(init_opts=opts.InitOpts(theme=ThemeType.WESTEROS))
    lineFig.set_global_opts(title_opts=opts.TitleOpts(title=title,subtitle=subtitle),
                            yaxis_opts=opts.AxisOpts(name=yName),
                            xaxis_opts=opts.AxisOpts(name=xName),
                            legend_opts=opts.LegendOpts(pos_left=pos_left))
    if isinstance(data, pd.Series):
        lineFig.add_xaxis(data.index.tolist())
        lineFig.add_yaxis(data.name, data.values.tolist())
    elif isinstance(data, pd.DataFrame):
        useCols = useCols if useCols else data.columns
        for i in range(len(data.columns)):
            lineFig.add_xaxis(data.index.astype("str").tolist())
            lineFig.add_yaxis(str(useCols[i]), data[data.columns[i]].fillna(0).values.tolist())
    # lineFig.set_series_opts(label_opts=opts.LabelOpts(is_show=False))
    return lineFig



# from pyecharts.render import make_snapshot
# from snapshot_selenium import snapshot
