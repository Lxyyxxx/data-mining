# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2019/10/9 13:11.
#

from urllib import request
from bs4 import BeautifulSoup
import pymongo
from datetime import datetime


# 连接数据库
client = pymongo.MongoClient('mongodb://207.148.6.90:27017')
db = client["一念永恒"]
col_chapter = db["chapter"]
col_info = db["info"]
# 网址
url = "https://www.biqukan.com"


def getSoup(url_: str):
    """
    得到html
    :param url_: 网页url
    :return: BeautifulSoup的html或None
    """
    try:
        headers = {"User-Agent":
                       "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                       "AppleWebKit/537.36 (KHTML, like Gecko) "
                       "Chrome/77.0.3865.90 Safari/537.36"}
        html = request.Request(url=url_, headers=headers)
        content = request.urlopen(html).read()
        soup_ = BeautifulSoup(content.decode('gbk'), features="html.parser")
        return soup_
    except:
        return None


def getInfo(soup_) -> None:
    """
    得到图书信息，并存入MongoDB数据库
    :param soup_: BeautifulSoup的html
    :return: 无
    """
    div_info = soup_.find("div", "info")
    # 封面url
    info = {"cover": url + div_info.find("img")["src"]}
    spans = div_info.find("div", "small").find_all("span")
    # 其他信息
    for span in spans:
        txt = span.get_text()
        txt_list = txt.split("：")
        info[txt_list[0]] = txt_list[1]
    # 存入MongoDB数据库
    col_info.insert_one(info)


def getChapter(soup_) -> None:
    """
    得到章节信息，并存入MongoDB数据库
    :param soup_: BeautifulSoup的html
    :return: 无
    """
    dds = soup_.find_all("dd")
    for dd in dds:
        chapter = {}
        tail = dd.find("a")["href"]
        url_next = url + tail
        # 如果url不存在在数据库中，就爬取并存入数据库中
        if col_chapter.count_documents({"url": url_next}) == 0:
            print(url_next)
            # 文章标题
            chapter["title"] = dd.get_text()
            # 下一篇文章的url
            chapter["url"] = url_next
            # 文章内容
            txt = getSoup(url_next).find("div", "showtxt").get_text()
            chapter["text"] = "\n".join(txt.split())
            # 保存时间
            chapter["save"] = datetime.now()
            # 存入MongoDB数据库
            print("save...")
            col_chapter.insert_one(chapter)


if __name__ == '__main__':
    soup = getSoup(url + "/1_1094/")
    getInfo(soup)
    getChapter(soup)
