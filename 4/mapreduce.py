# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2019/10/23 19:11.
#

import pymongo
from bson.son import SON

# 连接数据库
client = pymongo.MongoClient('mongodb://39.105.171.198:23333')
db = client["一念永恒"]

# 对命名实体进行统计
keys = ["cn", "tn", "pr"]
for i in keys:
    _id = "$" + i
    print(_id)
    pipeline = [
        {"$unwind": _id},
        {"$group": {"_id": _id, "count": {"$sum": 1}}},
        {"$sort": SON([("count", -1), ("_id", -1)])}
    ]
    db[i].insert_many(list(db.result.aggregate(pipeline)))
