# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2019/10/13 17:33.
#

import wordcloud
import matplotlib.pyplot as plt
import pymongo

# 连接数据库
client = pymongo.MongoClient('mongodb://39.105.171.198:23333')
db = client["一念永恒"]

# 命名实体名称
keys = ["cn", "tn", "pr"]
names = {"cn": "Chinese Name Recognition",
         "tn": "Transliteration Name Recognition",
         "pr": "Place Recognition"}
for i in keys:
    # 得到命名实体列表
    res = list()
    for j in db[i].find().sort("count", pymongo.DESCENDING).limit(100):
        res.append(j["_id"])
    words = " ".join(res)
    # 画词云
    wc = wordcloud.WordCloud(font_path="sarasa-ui-sc-regular.ttf",
                             max_words=100, background_color="white", scale=4).generate(words)
    plt.rcParams["savefig.dpi"] = 300
    plt.imshow(wc)
    plt.axis("off")
    plt.title(names[i])
    plt.savefig(names[i] + ".png")
    plt.show()
