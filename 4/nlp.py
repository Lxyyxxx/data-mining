# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2019/10/13 15:03.
#

import pyhanlp
import pymongo

# 连接数据库
client = pymongo.MongoClient('mongodb://39.105.171.198:23333')
db = client["一念永恒"]
col_chapter = db["chapter"]
col_result = db["result"]


def processing(chapter_dict: dict):
    """
    预处理，去除没用的信息和换行符
    :param chapter_dict: MongoDB存的字典
    :return: 文章正文、url
    """
    txt = chapter_dict["text"].strip(
        "请记住本书首发域名：www.biqukan.com。笔趣阁手机版阅读网址：wap.biqukan.com").replace(
        "(" + chapter_dict["url"] + ")chaptererror();", "").replace(
        "\n", "")
    return txt, chapter_dict["url"]


def ChineseSegment(txt: str) -> list:
    """
    中文分词
    :param txt: 文章正文
    :return: 词/词性 格式的列表（已去重）
    """
    words = set()
    for i in pyhanlp.HanLP.segment(txt):
        # 去除标点
        if str(i.nature) != "w":
            words.add(str(i.word)+"/"+str(i.nature))
    return list(words)


def ChineseNameRecognition(txt: str) -> list:
    """
    中文人名识别
    :param txt: 文章正文
    :return: 中文人名的列表
    """
    chinese_names = set()
    segment = pyhanlp.HanLP.newSegment().enableNameRecognize(True)
    for i in segment.seg(txt):
        # 中文人名
        if str(i.nature) == "nr":
            chinese_names.add(str(i.word))
    return list(chinese_names)


def TransliterationNameRecognition(txt: str) -> list:
    """
    音译人名识别
    :param txt: 文章正文
    :return: 音译人名的列表
    """
    transliterate_names = set()
    segment = pyhanlp.HanLP.newSegment().enableTranslatedNameRecognize(True)
    for i in segment.seg(txt):
        # 音译人名
        if str(i.nature) == "nrf":
            transliterate_names.add(str(i.word))
    return list(transliterate_names)


def PlaceRecognition(txt: str) -> list:
    """
    地名识别
    :param txt: 文章正文
    :return: 地名的列表
    """
    transliterate_names = set()
    segment = pyhanlp.HanLP.newSegment().enablePlaceRecognize(True)
    for i in segment.seg(txt):
        if str(i.nature) == "ns":
            transliterate_names.add(str(i.word))
    return list(transliterate_names)


def getChapter(count: int, last_id_: str) -> str:
    """
    遍历chapter表，处理后存到result表中
    :param count: 一次查询个数
    :param last_id_: 最后一次的id
    :return: 最后一次的id，用于下一次查询
    """
    for chapter_ in col_chapter.find({'_id': {'$gt': last_id_}}).limit(count):
        last_id_ = chapter_['_id']
        saveResult(chapter_)
    return last_id_


def saveResult(chapter: dict) -> None:
    """
    处理正文后存到result表中
    :param chapter: MongoDB存的字典
    :return: 无
    """
    res = dict()
    # url
    txt, res["_id"] = processing(chapter)
    # 中文分词
    res["words"] = ChineseSegment(txt)
    # 中文人名识别
    res["cn"] = ChineseNameRecognition(txt)
    # 音译人名识别
    res["tn"] = TransliterationNameRecognition(txt)
    # 地名识别
    res["pr"] = PlaceRecognition(txt)
    # 插入MongoDB
    col_result.insert_one(res)


if __name__ == '__main__':
    num = col_chapter.estimated_document_count()
    cnt = 20
    # 第一篇
    chapter = col_chapter.find_one()
    last_id = chapter["_id"]
    saveResult(chapter)
    # 后面的
    for _ in range(num // cnt + 1):
        last_id = getChapter(cnt, last_id)
