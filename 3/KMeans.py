# -*- coding: utf-8 -*- 
#
# Created by Xinyu on 2019/10/13 0:55.
#

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


dataset = pd.read_csv("dataset.csv").values


def L2(v1: np.ndarray, v2: np.ndarray) -> float:
    """
    计算L2范数，也就是欧几里得距离
    :param v1: 向量1
    :param v2: 向量2
    :return: L2范数
    """
    return np.linalg.norm(v1 - v2)


def randCentroid(k: int):
    """
    随机选择k个点作为类重心
    :param k: 聚类的数目k
    :return: 无 或者是 聚类重心坐标集合
    """
    # 如果k>数据集数量，返回无
    if k >= dataset.shape[0]:
        return None
    # 存聚类重心坐标
    centroids = np.zeros((k, 2))
    # 随机生成聚类坐标
    for j in range(2):
        _min = dataset[:, j].min()
        _range = dataset[:, j].max() - _min
        centroids[:, [j]] = _min + np.random.rand(k, 1)*_range
    return centroids


def KMeans(k: int):
    """
    划分聚类
    :param k: 聚类的数目k
    :return: 聚类重心坐标集合、每个点属于哪一类
    """
    # 随机生成聚类坐标
    centroids = randCentroid(k)
    num = dataset.shape[0]
    # 存每个点属于哪一类
    clusters = np.zeros(num)
    # 记住重心是否有改变
    changed = True
    while changed:
        changed = False
        for i in range(num):
            # 计算样本与各类重心的距离
            min_index, min_distance = -1, np.inf
            for j in range(k):
                distance = L2(dataset[i, :], centroids[j, :])
                if distance < min_distance:
                    min_index, min_distance = j, distance
            changed = True if clusters[i] != min_index else changed
            # 将样本划入相应的类
            clusters[i] = min_index
        # 计算并更新重心向量
        for i in range(k):
            all_index = np.where(clusters[:] == i)
            centroids[i, :] = np.mean(dataset[all_index], axis=0)
    return centroids, clusters


def showKMeans(k: int) -> None:
    """
    打印聚类结果
    :param k: 聚类的数目k
    :return: 无
    """
    centroids, clusters = KMeans(k)
    num = dataset.shape[0]
    plt.xlabel("属性1")
    plt.ylabel("属性2")
    # 画数据集的点
    mark = ['oy', 'om', 'or', 'og', 'ob', 'ok']
    for i in range(num):
        mark_index = int(clusters[i])
        plt.plot(dataset[i, 0], dataset[i, 1], mark[mark_index])
    # 画聚类重心
    mark = ['*y', '*m', '*r', '*g', '*b', '*k']
    for i in range(k):
        plt.plot(centroids[i, 0], centroids[i, 1], mark[i], markersize=12)
    # 保存结果
    plt.savefig("kmeans.png")
    plt.show()


if __name__ == '__main__':
    # k=2
    showKMeans(2)
